package net.sertik.cltforum.desktop;

import javafx.application.Application;

/**
 *
 * @author joeri
 */
public class Main {

    public static void main(String[] args) {
        Application.launch(net.sertik.cltforum.core.Main.class, args);
    }

}
