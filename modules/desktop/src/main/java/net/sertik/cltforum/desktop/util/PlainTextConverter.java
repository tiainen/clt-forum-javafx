package net.sertik.cltforum.desktop.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
//import org.datafx.reader.converter.InputStreamConverter;

/**
 *
 * @author joeri
 */
public class PlainTextConverter /*extends InputStreamConverter<String>*/ {

    private static final Logger LOG = Logger.getLogger(PlainTextConverter.class.getName());

    private String plainText;
/*
    @Override
    public void initialize(InputStream is) {
        BufferedReader reader = null;
        StringWriter writer = null;

        try {
            reader = new BufferedReader(new InputStreamReader(is));
            writer = new StringWriter();

            String line;
            while ((line = reader.readLine()) != null) {
                writer.write(line);
            }

            plainText = writer.toString();
        } catch (IOException ex) {
            LOG.log(Level.SEVERE, "Something went wrong while reading plain text from inputstream.", ex);
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException ex) {
                    LOG.log(Level.WARNING, "Something went wrong while closing inputstream reader.", ex);
                }
            }
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException ex) {
                    LOG.log(Level.WARNING, "Something went wrong while closing string writer.", ex);
                }
            }
        }
    }

    @Override
    public String get() {
        return plainText;
    }

    @Override
    public boolean next() {
        return false;
    }
*/
}
