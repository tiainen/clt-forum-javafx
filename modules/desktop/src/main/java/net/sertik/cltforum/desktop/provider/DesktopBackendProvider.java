package net.sertik.cltforum.desktop.provider;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import net.sertik.cltforum.core.provider.BackendProvider;
import net.sertik.cltforum.core.provider.BackendResponseHandler;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.client.LaxRedirectStrategy;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

/**
 *
 * @author joeri
 */
public class DesktopBackendProvider implements BackendProvider {

    private final CloseableHttpClient client;

    public DesktopBackendProvider() {
        client = HttpClients.custom()
                .setDefaultCookieStore(new BasicCookieStore())
                .setRedirectStrategy(new LaxRedirectStrategy())
                .build();
    }

    @Override
    public void initialize(BackendResponseHandler handler) {
        try {
            HttpGet request = new HttpGet("https://www.clt.be/CLTSite/index.php");
            String responseBody = client.execute(request, response -> {
                int status = response.getStatusLine().getStatusCode();
                if (status >= 200 && status < 300) {
                    HttpEntity entity = response.getEntity();
                    return entity != null ? EntityUtils.toString(entity, "UTF-8") : null;
                }
                throw new ClientProtocolException("Unexpected response status: " + status);
            });
            handler.onSuccess(responseBody);
        } catch (IOException ex) {
            handler.onFailure(ex.getMessage(), ex);
        }
    }

    @Override
    public void login(String username, String password, final BackendResponseHandler handler) {
        try {
            HttpPost request = new HttpPost("https://www.clt.be/CLTSite/index.php");
            List<NameValuePair> nvps = new LinkedList<>();
            nvps.add(new BasicNameValuePair("actie", "2"));
            nvps.add(new BasicNameValuePair("mycltusername", username));
            nvps.add(new BasicNameValuePair("mycltpassword", password));
            request.setEntity(new UrlEncodedFormEntity(nvps, "UTF-8"));

            String responseBody = client.execute(request, response -> {
                int status = response.getStatusLine().getStatusCode();
                if (status >= 200 && status < 300) {
                    HttpEntity entity = response.getEntity();
                    return entity != null ? EntityUtils.toString(entity, "UTF-8") : null;
                }
                throw new ClientProtocolException("Unexpected response status: " + status);
            });
            handler.onSuccess(responseBody);
        } catch (IOException ex) {
            handler.onFailure(ex.getMessage(), ex);
        }

//        RestSource<String> restSource = RestSourceBuilder.create()
//                .host("https://www.clt.be/")
//                .path("CLTSite/index.php")
//                .contentType("application/x-www-form-urlencoded")
//                .formParam("actie", "2")
//                .formParam("mycltusername", username)
//                .formParam("mycltpassword", password)
//                .requestMethod("POST")
//                .converter(new PlainTextConverter())
//                .build();
//
//        ObjectProperty<String> response = new SimpleObjectProperty<>();
//        Worker<String> service = ObjectDataProviderBuilder.create()
//                .dataReader(restSource)
//                .resultProperty(response)
//                .build()
//                .retrieve();
//
//        service.stateProperty().addListener((obs, ov, nv) -> {
//            if (nv.equals(Worker.State.FAILED)) {
//                handler.onFailure(null, service.getException());
//            } else if (nv.equals(Worker.State.SUCCEEDED)) {
//                handler.onSuccess(response.get());
//            }
//        });
    }

    @Override
    public void loadTopics(String forumLink, BackendResponseHandler handler) {
        try {
            HttpGet cvoRequest = new HttpGet(forumLink);
            client.execute(cvoRequest, response -> {
                int status = response.getStatusLine().getStatusCode();
                if (status >= 200 && status < 300) {
                    HttpEntity entity = response.getEntity();
                    return entity != null ? EntityUtils.toString(entity, "UTF-8") : null;
                }
                throw new ClientProtocolException("Unexpected response status: " + status);
            });

            HttpGet forumRequest = new HttpGet("http://www.e-lan.be/e-cvo/forum/forum.php");
            String forumResponseBody = client.execute(forumRequest, response -> {
                int status = response.getStatusLine().getStatusCode();
                if (status >= 200 && status < 300) {
                    HttpEntity entity = response.getEntity();
                    return entity != null ? EntityUtils.toString(entity, "UTF-8") : null;
                }
                throw new ClientProtocolException("Unexpected response status: " + status);
            });
            handler.onSuccess(forumResponseBody);
        } catch (IOException ex) {
            handler.onFailure(ex.getMessage(), ex);
        }
    }

}
