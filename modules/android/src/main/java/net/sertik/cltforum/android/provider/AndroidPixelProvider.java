package net.sertik.cltforum.android.provider;

import android.content.res.Resources;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import net.sertik.cltforum.core.provider.PixelProvider;

/**
 *
 * @author joeri
 */
public class AndroidPixelProvider implements PixelProvider {

    private final DisplayMetrics systemDisplayMetrics;

    public AndroidPixelProvider() {
        systemDisplayMetrics = Resources.getSystem().getDisplayMetrics();
    }

    @Override
    public double getPixels(double size) {
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, (float) size, systemDisplayMetrics);
    }

    @Override
    public double getFontPixels(double size) {
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, (float) size, systemDisplayMetrics);
    }

}
