package net.sertik.cltforum.android.provider;

import android.os.Looper;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.PersistentCookieStore;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.SyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;
import javafxports.android.FXActivity;
import net.sertik.cltforum.core.provider.BackendProvider;
import net.sertik.cltforum.core.provider.BackendResponseHandler;
import org.apache.http.Header;

/**
 *
 * @author joeri
 */
public class AndroidBackendProvider implements BackendProvider {

    private final AsyncHttpClient syncHttpClient = new SyncHttpClient();
    private final AsyncHttpClient asyncHttpClient = new AsyncHttpClient();

    public AndroidBackendProvider() {
        final PersistentCookieStore cookieStore = new PersistentCookieStore(FXActivity.getInstance());
        FXActivity.getInstance().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                getClient().setCookieStore(cookieStore);
            }
        });
    }

    @Override
    public void initialize(final BackendResponseHandler handler) {
        FXActivity.getInstance().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                getClient().get("https://www.clt.be/CLTSite/index.php", new TextHttpResponseHandler() {

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        handler.onFailure(responseString, throwable);
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, String responseString) {
                        handler.onSuccess(responseString);
                    }
                });
            }
        });
    }

    @Override
    public void login(final String username, final String password, final BackendResponseHandler handler) {
        FXActivity.getInstance().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                RequestParams params = new RequestParams();
                params.add("actie", "2");
                params.add("mycltusername", username);
                params.add("mycltpassword", password);
                getClient().post("https://www.clt.be/CLTSite/index.php", params, new TextHttpResponseHandler() {

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        handler.onFailure(responseString, throwable);
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, String responseString) {
                        handler.onSuccess(responseString);
                    }
                });
            }
        });
    }

    @Override
    public void loadTopics(final String forumLink, final BackendResponseHandler handler) {
        FXActivity.getInstance().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                getClient().get(forumLink, new TextHttpResponseHandler() {

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        handler.onFailure(responseString, throwable);
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, String responseString) {
                        getClient().get("http://www.e-lan.be/e-cvo/forum/forum.php", new TextHttpResponseHandler() {

                            @Override
                            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                                handler.onFailure(responseString, throwable);
                            }

                            @Override
                            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                                handler.onSuccess(responseString);
                            }
                        });
                    }
                });
            }
        });
    }

    /**
     * Get either synchronous or asynchronous instance of an http client
     * depending on the Looper status.
     *
     * @return a sychronous http client when myLooper is <code>null</code>
     */
    private AsyncHttpClient getClient() {
        if (Looper.myLooper() == null) {
            return syncHttpClient;
        }
        return asyncHttpClient;
    }

}
