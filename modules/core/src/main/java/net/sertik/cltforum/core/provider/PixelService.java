package net.sertik.cltforum.core.provider;

import static java.lang.String.format;
import java.util.Iterator;
import java.util.ServiceLoader;
import java.util.logging.Logger;

/**
 *
 * @author joeri
 */
public class PixelService implements PixelProvider {

    private static final Logger LOG = Logger.getLogger(PixelService.class.getName());

    private static PixelService instance;
    private final ServiceLoader<PixelProvider> loader;
    private PixelProvider provider;

    public PixelService() {
        loader = ServiceLoader.load(PixelProvider.class);

        Iterator<PixelProvider> iterator = loader.iterator();
        while (iterator.hasNext()) {
            if (provider == null) {
                provider = iterator.next();
                LOG.info(format("The following PixelProvider will be used: %s", provider.getClass().getName()));
            } else {
                LOG.info(format("This PixelProvider is ignored: %s", iterator.next().getClass().getName()));
            }
        }

        if (provider == null) {
            provider = new DefaultPixelProvider();
            LOG.warning(format("No PixelProvider implementation could be found, using %s.", provider.getClass().getName()));
        }
    }

    public static synchronized PixelService getInstance() {
        if (instance == null) {
            instance = new PixelService();
        }
        return instance;
    }

    @Override
    public double getPixels(double size) {
        return provider.getPixels(size);
    }

    @Override
    public double getFontPixels(double size) {
        return provider.getFontPixels(size);
    }

}
