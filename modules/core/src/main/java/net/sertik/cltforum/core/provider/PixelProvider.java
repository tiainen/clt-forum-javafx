package net.sertik.cltforum.core.provider;

/**
 *
 * @author joeri
 */
public interface PixelProvider {

    double getPixels(double size);

    double getFontPixels(double size);

}
