package net.sertik.cltforum.core.pane;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import net.sertik.cltforum.core.domain.ForumParser;
import net.sertik.cltforum.core.domain.Topic;
import net.sertik.cltforum.core.provider.BackendResponseHandler;
import net.sertik.cltforum.core.provider.BackendService;
import net.sertik.cltforum.core.provider.PixelService;

/**
 *
 * @author joeri
 */
public class ForumPane extends StackPane {

    private static final Logger LOG = Logger.getLogger(ForumPane.class.getName());

    private ObservableList<Topic> topics = FXCollections.observableArrayList();

    public ForumPane() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sdf.setTimeZone(TimeZone.getTimeZone("Europe/Brussels"));

        Topic sampleTopic1 = new Topic();
        sampleTopic1.setTitle("钟琴音乐会");
        sampleTopic1.setAuthor("Walter DE KLERCK");
        sampleTopic1.setId("30045");
        try {
            sampleTopic1.setDate(sdf.parse("2014-12-13 14:40:00"));
        } catch (ParseException ex) {
            sampleTopic1.setDate(new Date());
        }
        Topic sampleTopic2 = new Topic();
        sampleTopic2.setTitle("2015去中国学习怎么样");
        sampleTopic2.setAuthor("Thomas LINSINGER");
        sampleTopic2.setId("28237");
        try {
            sampleTopic2.setDate(sdf.parse("2014-11-08 21:05:00"));
        } catch (ParseException ex) {
            sampleTopic2.setDate(new Date());
        }
        Topic sampleTopic3 = new Topic();
        sampleTopic3.setTitle("NüDE");
        sampleTopic3.setAuthor("Simon Vleugels");
        sampleTopic3.setId("28240");
        try {
            sampleTopic3.setDate(sdf.parse("2014-11-08 21:48:00"));
        } catch (ParseException ex) {
            sampleTopic3.setDate(new Date());
        }
        topics.addAll(sampleTopic1, sampleTopic2, sampleTopic3);

        buildControls();
    }

    public ForumPane(String forumLink) {
        buildControls();

        loadForumPage(forumLink);
    }

    private void buildControls() {
        String fontName = "Droid Sans Fallback";
        double titleFontSize = PixelService.getInstance().getFontPixels(32.0);

        Label title = new Label("CLT Forum");
        title.setFont(Font.font(fontName, titleFontSize));

        ListView<Topic> topicsList = new ListView<>();
        topicsList.setCellFactory((listView) -> new TopicListCell());
        topicsList.setItems(topics);

        VBox controls = new VBox(PixelService.getInstance().getPixels(15.0));
        controls.setPadding(new Insets(PixelService.getInstance().getPixels(20.0)));
        controls.setAlignment(Pos.CENTER);
        controls.getChildren().addAll(title, topicsList);
        VBox.setVgrow(topicsList, Priority.ALWAYS);

        getChildren().add(controls);
    }

    private void loadForumPage(String forumLink) {
        BackendService.getInstance().loadTopics(forumLink, new BackendResponseHandler() {

            @Override
            public void onFailure(String responseString, Throwable throwable) {
                LOG.log(Level.WARNING, "Failed to call BackendService.login", throwable);
            }

            @Override
            public void onSuccess(String responseString) {
                parseTopicsResponse(responseString);
            }
        });
    }

    private void parseTopicsResponse(String response) {
        topics.clear();

        List<Topic> parsedTopics = ForumParser.parse(response);
        if (parsedTopics != null) {
            Collections.sort(parsedTopics, (t1, t2) -> {
                if (t1.getDate() != null && t2.getDate() != null) {
                    return t1.getDate().compareTo(t2.getDate()) * -1;
                }
                return t1.getId().compareTo(t2.getId());
            });
            topics.addAll(parsedTopics);
        }
    }
}
