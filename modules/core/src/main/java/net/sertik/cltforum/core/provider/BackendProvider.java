package net.sertik.cltforum.core.provider;

/**
 *
 * @author joeri
 */
public interface BackendProvider {

    void initialize(final BackendResponseHandler handler);

    void login(String username, String password, BackendResponseHandler handler);

    void loadTopics(String forumLink, BackendResponseHandler handler);

}
