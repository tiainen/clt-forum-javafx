package net.sertik.cltforum.core.provider;

/**
 *
 * @author joeri
 */
public interface BackendResponseHandler {

    void onFailure(String responseString, Throwable throwable);

    void onSuccess(String responseString);

}
