package net.sertik.cltforum.core.domain;

import java.util.Date;

/**
 *
 * @author joeri
 */
public class Topic {

    private String id;
    private Date date;
    private String title;
    private String content;
    private String author;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    @Override
    public String toString() {
        return "Topic{" + "id=" + id + ", date=" + date + ", title=" + title + ", content=" + content + ", author=" + author + '}';
    }

}
