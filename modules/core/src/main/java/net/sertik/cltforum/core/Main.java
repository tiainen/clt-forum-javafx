package net.sertik.cltforum.core;

import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Screen;
import javafx.stage.Stage;
import net.sertik.cltforum.core.pane.ForumPane;
import net.sertik.cltforum.core.pane.LoginPane;
import net.sertik.cltforum.core.provider.BackendResponseHandler;
import net.sertik.cltforum.core.provider.BackendService;

/**
 *
 * @author Joeri
 */
public class Main extends Application {

    private static final Logger LOG = Logger.getLogger(Main.class.getName());

    @Override
    public void start(Stage primaryStage) throws Exception {
        Screen primaryScreen = Screen.getPrimary();
        final Rectangle2D visualBounds = primaryScreen.getVisualBounds();

        BackendService.getInstance().initialize(new BackendResponseHandler() {

            @Override
            public void onFailure(String responseString, Throwable throwable) {
                LOG.log(Level.WARNING, "Calling BackendProvider.initialize() failed: " + responseString, throwable);

                Platform.runLater(() -> {
                    LoginPane pane = new LoginPane();
                    Scene scene = new Scene(pane, visualBounds.getWidth(), visualBounds.getHeight());

                    primaryStage.setScene(scene);
                    primaryStage.show();
                });
            }

            @Override
            public void onSuccess(String responseString) {
                String forumLink = parseInitialResponse(responseString);

                Platform.runLater(() -> {
                    Pane pane;
                    if (forumLink == null) {
                        pane = new LoginPane();
                    } else {
                        pane = new ForumPane(forumLink);
                    }

                    Scene scene = new Scene(pane, visualBounds.getWidth(), visualBounds.getHeight());

                    primaryStage.setScene(scene);
                    primaryStage.show();
                });
            }
        });

//        Platform.runLater(() -> {
//            primaryStage.setScene(new Scene(new ForumPane(), visualBounds.getWidth(), visualBounds.getHeight()));
//            primaryStage.show();
//        });
    }

    private String parseInitialResponse(String response) {
        if (response == null || response.trim().isEmpty()) {
            return null;
        }

        String accessToCVO = " voor toegang tot het online leerplatform e-CLT";
        int idxAccessToCVO = response.indexOf(accessToCVO);
        if (idxAccessToCVO == -1) {
            return null;
        }

        String linkToCVO = "<a href=\"http://www.e-lan.be/e-cvo/ajax/check_login_CLT.php?email=";
        int idxLinkToCVO = response.indexOf(linkToCVO);
        if (idxLinkToCVO != -1) {
            return response.substring(idxLinkToCVO + 9, response.indexOf("\">Klik hier</a>", idxLinkToCVO));
        }

        return null;
    }

}
