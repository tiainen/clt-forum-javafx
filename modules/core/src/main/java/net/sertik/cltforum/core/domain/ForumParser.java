package net.sertik.cltforum.core.domain;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author joeri
 */
public class ForumParser {

    private static final Logger LOG = Logger.getLogger(ForumParser.class.getName());

    private static final SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy   HH:mm");
    static {
        sdf.setTimeZone(TimeZone.getTimeZone("Europe/Brussels"));
    }

    public static List<Topic> parse(String html) {
        int idxBody = html.indexOf("<BODY>");
        if (idxBody == -1) {
            LOG.log(Level.WARNING, "String '<BODY>' not found.");
            return null;
        }

        int idxTable = html.indexOf("<table ", idxBody);
        if (idxTable == -1) {
            LOG.log(Level.WARNING, "String '<table ' not found.");
            return null;
        }

        int idxRowEnd = html.indexOf("</tr>", idxTable);
        if (idxRowEnd == -1) {
            LOG.log(Level.WARNING, "String '</tr>' not found.");
            return null;
        }

        int idxTableEnd = html.indexOf("</table>", idxRowEnd);
        if (idxTableEnd == -1) {
            LOG.log(Level.WARNING, "String </table> not found.");
            return null;
        }

        List<Topic> topics = new ArrayList<>();

        int idxNextRow;
        while ((idxNextRow = html.indexOf("<tr ", idxRowEnd)) != -1 && idxNextRow < idxTableEnd) {
            idxRowEnd = html.indexOf("</tr>", idxNextRow);

            String row = html.substring(idxNextRow, idxRowEnd + "</tr>".length());
            LOG.log(Level.INFO, "{0}", row);

            int idxSpanTitleBegin = row.indexOf("<span id='");
            if (idxSpanTitleBegin == -1) {
                LOG.log(Level.WARNING, "String '<span id='' not found.");
                continue;
            }

            int idxSpanTitleEnd = row.indexOf("'>", idxSpanTitleBegin);
            if (idxSpanTitleEnd == -1) {
                LOG.log(Level.WARNING, "String ''>' not found.");
                continue;
            }

            Topic topic = new Topic();
            topics.add(topic);

            String id = row.substring(idxSpanTitleBegin + "<span id='".length(), idxSpanTitleEnd);
            topic.setId(id);

            int idxSpanTitleAfter = row.indexOf("</span>", idxSpanTitleEnd);
            if (idxSpanTitleAfter == -1) {
                LOG.log(Level.WARNING, "String '</span>' not found.");
                continue;
            }

            String title = row.substring(idxSpanTitleEnd + "'>".length(), idxSpanTitleAfter);
            topic.setTitle(title);

            int idxTopicByStart = row.indexOf("door <b>", idxSpanTitleAfter);
            if (idxTopicByStart == -1) {
                LOG.log(Level.WARNING, "String 'door <b>' not found.");
                continue;
            }

            int idxTopicByEnd = row.indexOf("</b>", idxTopicByStart);
            if (idxTopicByEnd == -1) {
                LOG.log(Level.WARNING, "String '<b/> not found.");
                continue;
            }

            String author = row.substring(idxTopicByStart + "door <b>".length(), idxTopicByEnd);
            topic.setAuthor(author);

            int idxDateStart = row.indexOf(" - ", idxTopicByEnd);
            if (idxDateStart == -1) {
                LOG.log(Level.WARNING, "String ' - ' not found.");
                continue;
            }

            int idxDateEnd = row.indexOf("</td>", idxDateStart);
            if (idxDateEnd == -1) {
                LOG.log(Level.WARNING, "String '</td>' not found.");
                continue;
            }

            String dateString = row.substring(idxDateStart + " - ".length(), idxDateEnd);
            try {
                Date date = sdf.parse(dateString); 
                topic.setDate(date);
            } catch (ParseException ex) {
                LOG.log(Level.WARNING, "Error parsing date string: " + dateString, ex);
                continue;
            }
        }

        return topics;
    }

}
