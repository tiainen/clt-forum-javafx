package net.sertik.cltforum.core.pane;

import java.text.SimpleDateFormat;
import java.util.TimeZone;
import javafx.geometry.HPos;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import net.sertik.cltforum.core.domain.Topic;
import net.sertik.cltforum.core.provider.PixelService;

/**
 *
 * @author joeri
 */
public class TopicListCell extends ListCell<Topic> {

    private static final SimpleDateFormat sdf = new SimpleDateFormat("MMM dd");
    static {
        sdf.setTimeZone(TimeZone.getTimeZone("Europe/Brussels"));
    }

    private final double largerFontSize = PixelService.getInstance().getFontPixels(24.0);
    private final double normalFontSize = PixelService.getInstance().getFontPixels(18.0);

    private final VBox pane;
    private final Label title;
    private final Label author;
    private final Label date;

    public TopicListCell() {
        String fontName = "Droid Sans Fallback";

        title = new Label();
        title.setFont(Font.font(fontName, largerFontSize));

        author = new Label();
        author.setFont(Font.font(fontName, normalFontSize));

        date = new Label();
        date.setFont(Font.font(fontName, normalFontSize));

        GridPane details = new GridPane();
        details.addRow(0, author, date);
        GridPane.setHgrow(author, Priority.ALWAYS);
        GridPane.setHalignment(author, HPos.LEFT);
        GridPane.setHgrow(date, Priority.ALWAYS);
        GridPane.setHalignment(date, HPos.RIGHT);
        

        pane = new VBox(5.0);
        pane.getChildren().addAll(title, details);

        pane.setStyle("border-bottom: 1px solid grey");
    }

    @Override
    protected void updateItem(Topic item, boolean empty) {
        super.updateItem(item, empty);

        if (item != null) {
            title.setText(item.getTitle());
            author.setText(item.getAuthor());
            date.setText(sdf.format(item.getDate()));
            setGraphic(pane);
        } else {
            setGraphic(null);
        }
    }

}
