package net.sertik.cltforum.core.provider;

/**
 *
 * @author joeri
 */
public class DefaultPixelProvider implements PixelProvider {

    @Override
    public double getPixels(double size) {
        return size;
    }

    @Override
    public double getFontPixels(double size) {
        return size;
    }
    
}
