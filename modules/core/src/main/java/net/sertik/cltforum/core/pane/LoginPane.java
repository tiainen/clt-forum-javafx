package net.sertik.cltforum.core.pane;

import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.TextField;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import net.sertik.cltforum.core.provider.BackendResponseHandler;
import net.sertik.cltforum.core.provider.BackendService;
import net.sertik.cltforum.core.provider.PixelService;

/**
 *
 * @author joeri
 */
public class LoginPane extends StackPane {

    private static final Logger LOG = Logger.getLogger(LoginPane.class.getName());

    private final Label message = new Label();
    private final ProgressIndicator loader = new ProgressIndicator();

    public LoginPane() {
        buildControls();
    }

    private void buildControls() {
        String fontName = "Droid Sans Fallback";
        double titleFontSize = PixelService.getInstance().getFontPixels(32.0);
        double normalFontSize = PixelService.getInstance().getFontPixels(18.0);

        Label title = new Label("Sign in to CLT Forum");
        title.setFont(Font.font(fontName, titleFontSize));

        message.setFont(Font.font(fontName, normalFontSize));
        message.setVisible(false);

        loader.setPrefHeight(PixelService.getInstance().getPixels(48.0));
        loader.setPrefWidth(PixelService.getInstance().getPixels(48.0));
        loader.setVisible(false);
        StackPane messageAndLoader = new StackPane(message, loader);

        TextField username = new TextField();
        username.setFont(Font.font(fontName, normalFontSize));
        username.setPromptText("username");

        PasswordField password = new PasswordField();
        password.setFont(Font.font(fontName, normalFontSize));
        password.setPromptText("password");

        Button login = new Button("sign in");
        login.setFont(Font.font(fontName, normalFontSize));
        login.setOnAction(e -> {
            message.setVisible(false);
            message.setTextFill(Color.rgb(225, 25, 25));
            loader.setVisible(true);

            BackendService.getInstance().login(username.getText(), password.getText(), new BackendResponseHandler() {

                @Override
                public void onFailure(String responseString, Throwable throwable) {
                    LOG.log(Level.WARNING, "Failed to call BackendService.login", throwable);
                    loader.setVisible(false);
                    message.setVisible(true);
                    message.setText("Something unexpected happened while signing in. Try again?");
                }

                @Override
                public void onSuccess(String responseString) {
                    loader.setVisible(false);
                    parseLoginResponse(responseString);
                }
            });
        });

        VBox controls = new VBox(PixelService.getInstance().getPixels(15.0));
        controls.setPadding(new Insets(PixelService.getInstance().getPixels(20.0)));
        controls.setAlignment(Pos.CENTER);
        controls.getChildren().addAll(title, messageAndLoader, username, password, login);

        getChildren().add(controls);
    }

    private void parseLoginResponse(String response) {
        if (response == null || response.trim().isEmpty()) {
            message.setText("Invalid login credentials provided. Try again?");
            message.setVisible(true);
            return;
        }

        String accessToCVO = " voor toegang tot het online leerplatform e-CLT";
        int idxAccessToCVO = response.indexOf(accessToCVO);
        if (idxAccessToCVO == -1) {
            message.setText("Invalid login credentials provided. Try again?");
            message.setVisible(true);
            return;
        }

        String linkToCVO = "<a href=\"http://www.e-lan.be/e-cvo/ajax/check_login_CLT.php?email=";
        int idxLinkToCVO = response.indexOf(linkToCVO);
        if (idxLinkToCVO != -1) {
            String forumLink = response.substring(idxLinkToCVO + 9, response.indexOf("\">Klik hier</a>", idxLinkToCVO));

            Platform.runLater(() -> {
                Stage stage = (Stage) getScene().getWindow();
                stage.setScene(new Scene(new ForumPane(forumLink), getScene().getWidth(), getScene().getHeight()));
            });
        }

    }
}
