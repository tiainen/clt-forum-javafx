package net.sertik.cltforum.core.provider;

import static java.lang.String.format;
import java.util.Iterator;
import java.util.ServiceLoader;
import java.util.logging.Logger;

/**
 *
 * @author joeri
 */
public class BackendService implements BackendProvider {

    private static final Logger LOG = Logger.getLogger(BackendService.class.getName());

    private static BackendService instance;
    private final ServiceLoader<BackendProvider> loader;
    private BackendProvider provider;

    public BackendService() {
        loader = ServiceLoader.load(BackendProvider.class);

        Iterator<BackendProvider> iterator = loader.iterator();
        while (iterator.hasNext()) {
            if (provider == null) {
                provider = iterator.next();
                LOG.info(format("The following BackendProvider will be used: %s", provider.getClass().getName()));
            } else {
                LOG.info(format("This BackendProvider is ignored: %s", iterator.next().getClass().getName()));
            }
        }

        if (provider == null) {
            LOG.severe(format("No BackendProvider implementation could be found!", provider.getClass().getName()));
        }
    }

    public static synchronized BackendService getInstance() {
        if (instance == null) {
            instance = new BackendService();
        }
        return instance;
    }

    @Override
    public void initialize(BackendResponseHandler handler) {
        provider.initialize(handler);
    }

    @Override
    public void login(String username, String password, BackendResponseHandler handler) {
        provider.login(username, password, handler);
    }

    @Override
    public void loadTopics(String forumLink, BackendResponseHandler handler) {
        LOG.info(format("Loading CVO forum with link: %s", forumLink));
        provider.loadTopics(forumLink, handler);
    }

}
