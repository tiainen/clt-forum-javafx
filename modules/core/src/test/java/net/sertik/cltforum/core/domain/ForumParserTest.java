package net.sertik.cltforum.core.domain;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;
import static org.testng.Assert.assertEquals;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.testng.reporters.Files;

/**
 *
 * @author joeri
 */
public class ForumParserTest {

    public String forumText;

    @BeforeTest
    public void beforeTest() {
        try (InputStream is = ForumParserTest.class.getResourceAsStream("/pages/forum.txt")) {
            forumText = Files.readFile(is);
        } catch (IOException ex) {
            Logger.getLogger(ForumParserTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Test
    public void testParseForumPage() throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sdf.setTimeZone(TimeZone.getTimeZone("Europe/Brussels"));

        List<Topic> topics = ForumParser.parse(forumText);
        assertEquals(topics.size(), 20);

        assertEquals(topics.get(0).getId(), "30045");
        assertEquals(topics.get(0).getTitle(), "钟琴音乐会");
        assertEquals(topics.get(0).getAuthor(), "Walter DE KLERCK");
        assertEquals(topics.get(0).getDate(), sdf.parse("2014-12-13 14:40:00"));

        assertEquals(topics.get(1).getId(), "28237");
        assertEquals(topics.get(1).getTitle(), "2015去中国学习怎么样？");
        assertEquals(topics.get(1).getAuthor(), "Thomas LINSINGER");
        assertEquals(topics.get(1).getDate(), sdf.parse("2014-11-08 21:05:00"));

        assertEquals(topics.get(19).getId(), "28240");
        assertEquals(topics.get(19).getTitle(), "NüDE");
        assertEquals(topics.get(19).getAuthor(), "Simon Vleugels");
        assertEquals(topics.get(19).getDate(), sdf.parse("2014-11-08 21:48:00"));
    }

}
